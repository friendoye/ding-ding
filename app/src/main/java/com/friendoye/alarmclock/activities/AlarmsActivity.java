package com.friendoye.alarmclock.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.friendoye.alarmclock.R;
import com.friendoye.alarmclock.database.DatabaseHelper;
import com.friendoye.alarmclock.fragments.AlarmOptionsFragment;
import com.friendoye.alarmclock.fragments.AlarmsFragment;
import com.friendoye.alarmclock.services.AlarmService;

import java.util.GregorianCalendar;


public class AlarmsActivity extends AppCompatActivity
        implements AlarmsFragment.OnAlarmActivityChanged,
        AlarmOptionsFragment.OnOptionSelectedListener {
    public static final String REQUEST_KEY = "request_key";
    public static final String ID_KEY = "id_key";
    public static final int CREATE_ALARM_REQUEST = 10387;
    public static final int UPDATE_ALARM_REQUEST = 8356;

    private DatabaseHelper mDatabaseHelper;
    private AlarmsFragment mAlarmsFragment;
    private AlarmManager mAlarmManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarms);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);

        View fabView = findViewById(R.id.fab);
        fabView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAlarmCreation();
            }
        });

        mDatabaseHelper = new DatabaseHelper(getApplicationContext());
        mAlarmsFragment = (AlarmsFragment) getFragmentManager()
                .findFragmentById(R.id.alarms_fragment);
        mAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        refreshAlarms();
    }

    @Override
    public void onAlarmActivityChanged(int id, long time, boolean activated) {
        if (activated) {
            registerAlarm(id, time);
        } else {
            unregisterAlarm(id, time);
        }

        mDatabaseHelper.updateAlarmActivity(id, activated);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            long time = data.getLongExtra(AlarmCreationActivity.TIME_KEY, 0);
            long id = data.getLongExtra(AlarmCreationActivity.ID_KEY, 0);
            if (requestCode == UPDATE_ALARM_REQUEST) {
                unregisterAlarm(id, time);
            }
            registerAlarm(id, time);
            refreshAlarms();
        }
    }

    protected void refreshAlarms() {
        //if (mDatabaseHelper.hasAlarms()) {
        mAlarmsFragment.setCursor(mDatabaseHelper.getAlarms());
        //}
    }

    @Override
    public void changeAlarm(long id) {
        Intent intent = new Intent(this, AlarmCreationActivity.class);
        intent.putExtra(ID_KEY, id);
        startActivityForResult(intent, UPDATE_ALARM_REQUEST);
    }

    protected void startAlarmCreation() {
        Intent intent = new Intent(this, AlarmCreationActivity.class);
        startActivityForResult(intent, CREATE_ALARM_REQUEST);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        intent.putExtra(REQUEST_KEY, requestCode);
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    public void deleteAlarm(long id) {
        mDatabaseHelper.deleteAlarm(id);
        refreshAlarms();
    }

    protected void registerAlarm(long id, long time) {
        if (checkTime(time)) {
            Intent intent = new Intent(this, AlarmService.class);
            intent.putExtra(AlarmService.ID_KEY, id);
            intent.putExtra(AlarmService.TIME_KEY, time);
            PendingIntent alarmIntent = PendingIntent.getService(this, 0, intent, 0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, time, alarmIntent);
            } else {
                mAlarmManager.set(AlarmManager.RTC_WAKEUP, time, alarmIntent);
            }
        }
    }

    protected void unregisterAlarm(long id, long time) {
        Intent intent = new Intent(this, AlarmService.class);
        intent.putExtra(AlarmService.ID_KEY, id);
        intent.putExtra(AlarmService.TIME_KEY, time);
        PendingIntent alarmIntent = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        alarmIntent.cancel();
        mAlarmManager.cancel(alarmIntent);
    }

    /**
     * Checks whether a given time passed. Return true, if it's not.
     */
    protected boolean checkTime(long time) {
        long currentTime = new GregorianCalendar().getTimeInMillis();
        return time > currentTime;
    }
}
