package com.friendoye.alarmclock.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.friendoye.alarmclock.R;
import com.friendoye.alarmclock.database.DatabaseHelper;
import com.friendoye.alarmclock.fragments.AlarmCreationFragment;
import com.friendoye.alarmclock.fragments.DetailsFragment;
import com.friendoye.alarmclock.utils.Alarm;
import com.friendoye.alarmclock.utils.DifficultyType;
import com.friendoye.alarmclock.utils.DisableType;

public class AlarmCreationActivity extends AppCompatActivity
        implements DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener {
    public static final String TIME_KEY = "time_key";
    public static final String ID_KEY = "id_key";

    private AlarmCreationFragment mCreationFragment;
    private DatabaseHelper mDatabaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_creation);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.set_up_alarm_text);
        setSupportActionBar(toolbar);

        mDatabaseHelper = new DatabaseHelper(getApplicationContext());

        if (mCreationFragment == null) {
            mCreationFragment = (AlarmCreationFragment) getFragmentManager()
                    .findFragmentById(R.id.creation_fragment);
        }

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            if (intent.getIntExtra(AlarmsActivity.REQUEST_KEY, 0)
                    == AlarmsActivity.UPDATE_ALARM_REQUEST) {
                long id = intent.getLongExtra(AlarmsActivity.ID_KEY, -1);
                Alarm alarm = mDatabaseHelper.getAlarm(id);
                mCreationFragment.setAlarmInfo(alarm);
                restoreDetailsDefaults(alarm.difficulty,
                        alarm.disable);
                saveAlarmId(id);
            } else {
                restoreDetailsDefaults(DifficultyType.getDefault(),
                        DisableType.getDefault());
                saveAlarmId(-1);
            }
        }

        findViewById(R.id.saveButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAlarm();
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        mCreationFragment.setDate(year, monthOfYear, dayOfMonth);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        mCreationFragment.setTime(hourOfDay, minute);
    }

    protected void createAlarm() {
        Alarm alarm = mCreationFragment.createAlarm();
        long id = retrieveAlarmId();
        if (id == -1) {
            id = mDatabaseHelper.insertAlarm(alarm);
        } else {
            mDatabaseHelper.updateAlarm(id, alarm);
        }
        // Notify calling Activity about success
        Intent intent = new Intent();
        intent.putExtra(TIME_KEY, alarm.time);
        intent.putExtra(ID_KEY, id);
        setResult(RESULT_OK, intent);
        finish();
    }

    protected void saveAlarmId(long id) {
        SharedPreferences.Editor editor =
                PreferenceManager.getDefaultSharedPreferences(this).edit();

        editor.putLong(ID_KEY, id);

        editor.apply();
    }

    protected long retrieveAlarmId() {
        return PreferenceManager.getDefaultSharedPreferences(this)
                .getLong(ID_KEY, -1);
    }

    protected void restoreDetailsDefaults(DifficultyType difficulty,
                                          DisableType disable) {
        SharedPreferences.Editor editor =
                PreferenceManager.getDefaultSharedPreferences(this).edit();

        editor.putString(DetailsFragment.DIFFICULTY_KEY,
                difficulty.toString());
        editor.putString(DetailsFragment.DISABLE_KEY,
                disable.toString());

        editor.apply();
    }
}