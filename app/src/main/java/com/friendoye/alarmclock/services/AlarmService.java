package com.friendoye.alarmclock.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;

import com.friendoye.alarmclock.R;
import com.friendoye.alarmclock.database.DatabaseHelper;
import com.friendoye.alarmclock.utils.Alarm;

public class AlarmService extends IntentService {
    public static final String ID_KEY = "id_key";
    public static final String TIME_KEY = "time_key";
    public static final String GROUP_KEY = "alarm_group";
    public static final String SERVICE_NAME = "ALARM SERVICE";

    private DatabaseHelper mDatabaseHelper;

    public AlarmService() {
        super(SERVICE_NAME);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        long id = intent.getLongExtra(ID_KEY, 0);
        long time = intent.getLongExtra(TIME_KEY, 0);
        mDatabaseHelper = new DatabaseHelper(getApplicationContext());
        Alarm alarm = mDatabaseHelper.getAlarm(id);

        if (alarm.time != time) {
            return;
        }

        Notification.Builder builder = new Notification.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(alarm.description)
                .setSmallIcon(R.drawable.ic_alarm_grey600_24dp)
                .setAutoCancel(true);
        Notification notification;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setGroup(GROUP_KEY);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            notification = builder.build();
        } else {
            notification = builder.getNotification();
        }

        NotificationManager manager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        manager.notify((int) id, notification);
    }
}
