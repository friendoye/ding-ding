package com.friendoye.alarmclock.fragments;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.friendoye.alarmclock.R;
import com.friendoye.alarmclock.utils.Alarm;
import com.friendoye.alarmclock.utils.DifficultyType;
import com.friendoye.alarmclock.utils.DisableType;
import com.friendoye.alarmclock.utils.RussianDateFormat;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class AlarmCreationFragment extends PreferenceFragment {
    public static final String REPEAT_KEY = "repeat";
    public static final String ACTIVATED_KEY = "activated";
    public static final String TIME_KEY = "time_key";

    private EditText mDescriptionEditText;
    private TextView mDateSpinner;
    private TextView mTimeSpinner;
    private Calendar mCalendar;

    public AlarmCreationFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_preferences);

        if (mCalendar == null) {
            mCalendar = new GregorianCalendar();
        }

        if (savedInstanceState != null) {
            mCalendar.setTimeInMillis(savedInstanceState.getLong(TIME_KEY, 0));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View prefView = super.onCreateView(inflater,
                container, savedInstanceState);
        ListView prefList = (ListView) prefView.findViewById(android.R.id.list);
        View headerView = createHeaderView(inflater, prefList);
        prefList.addHeaderView(headerView);
        return prefView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Activity activity = getActivity();
        ListView list = (ListView) activity.findViewById(android.R.id.list);

        list.setDivider(ContextCompat.getDrawable(getActivity(),
                R.drawable.list_divider_with_padding));
        final int dividerHeight = (int)
                (2 * getResources().getDisplayMetrics().density);
        list.setDividerHeight(dividerHeight);

        list.setHeaderDividersEnabled(false);
        list.setFooterDividersEnabled(true);
        list.setPadding(0, 0, 0, 0);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(TIME_KEY, mCalendar.getTimeInMillis());
    }

    private View createHeaderView(LayoutInflater inflater, ListView list) {
        View rootView = inflater.inflate(R.layout.settings_header,
                list, false);

        mDescriptionEditText = (EditText) rootView
                .findViewById(R.id.descriptionEditText);

        mDateSpinner = (TextView) rootView.findViewById(R.id.dateEditText);
        mDateSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        mTimeSpinner = (TextView) rootView.findViewById(R.id.timeEditText);
        mTimeSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog();
            }
        });

        setDate();
        setTime();

        return rootView;
    }

    public Alarm createAlarm() {
        SharedPreferences prefs =
                PreferenceManager.getDefaultSharedPreferences(getActivity());
        Alarm alarm = new Alarm();
        // Initialization
        alarm.description = mDescriptionEditText.getText().toString();
        alarm.difficulty = DifficultyType.valueOf(
                prefs.getString(DetailsFragment.DIFFICULTY_KEY, ""));
        alarm.disable = DisableType.valueOf(
                prefs.getString(DetailsFragment.DISABLE_KEY, ""));
        alarm.repeat = prefs.getBoolean(REPEAT_KEY, false);
        alarm.activated = prefs.getBoolean(ACTIVATED_KEY, true);
        prefs.edit().putBoolean(ACTIVATED_KEY, true).apply();
        alarm.time = mCalendar.getTimeInMillis();
        // TODO: Initialize "melody"

        return alarm;
    }

    public void setAlarmInfo(Alarm alarm) {
        SharedPreferences.Editor editor = PreferenceManager
                .getDefaultSharedPreferences(getActivity())
                .edit();

        mDescriptionEditText.setText(alarm.description);
        editor.putString(DetailsFragment.DIFFICULTY_KEY,
                alarm.difficulty.toString());
        editor.putString(DetailsFragment.DISABLE_KEY,
                alarm.disable.toString());
        editor.putBoolean(AlarmCreationFragment.REPEAT_KEY, alarm.repeat);
        editor.putBoolean(ACTIVATED_KEY, alarm.activated);
        mCalendar.setTimeInMillis(alarm.time);
        setDate();
        setTime();
        // TODO: Initialize "melody"

        editor.apply();
        notifyPreferenceChanged();
    }

    protected void notifyPreferenceChanged() {
        BaseAdapter settingsAdapter = (BaseAdapter) getPreferenceScreen()
                .getRootAdapter();
        settingsAdapter.notifyDataSetChanged();
    }

    protected void showDatePickerDialog() {
        DialogFragment newFragment = new DatePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(DatePickerFragment.DATE_KEY,
                mCalendar.getTimeInMillis());
        newFragment.setArguments(bundle);
        newFragment.show(getFragmentManager(), "datePicker");
    }

    protected void showTimePickerDialog() {
        DialogFragment newFragment = new TimePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(TimePickerFragment.TIME_KEY,
                mCalendar.getTimeInMillis());
        newFragment.setArguments(bundle);
        newFragment.show(getFragmentManager(), "timePicker");
    }

    public void setDate(int year, int month, int dayOfMonth) {
        mCalendar.set(year, month, dayOfMonth);
        setDate();
    }

    protected void setDate() {
        Locale locale = Locale.getDefault();
        SimpleDateFormat formatter;
        if (locale.getLanguage().equals("ru")) {
            DateFormatSymbols dateFormat = new DateFormatSymbols();
            dateFormat.setShortMonths(RussianDateFormat.MONTH_NAMES);
            dateFormat.setShortWeekdays(RussianDateFormat.DAY_NAMES);
            formatter = new SimpleDateFormat("EEE, MMM dd, yyyy");
            formatter.setDateFormatSymbols(dateFormat);
        } else {
            formatter = new SimpleDateFormat("EEE, MMM dd, yyyy", Locale.ENGLISH);
        }
        String text = formatter.format(mCalendar.getTime());
        mDateSpinner.setText(text);
    }

    public void setTime(int hours, int minutes) {
        mCalendar.set(Calendar.HOUR_OF_DAY, hours);
        mCalendar.set(Calendar.MINUTE, minutes);
        setTime();
    }

    protected void setTime() {
        Locale locale = Locale.getDefault();
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm", locale);
        mTimeSpinner.setText(formatter.format(mCalendar.getTime()));
    }
}
