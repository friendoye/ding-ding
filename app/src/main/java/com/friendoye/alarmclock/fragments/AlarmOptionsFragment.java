package com.friendoye.alarmclock.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.friendoye.alarmclock.R;

/**
 *
 */
public class AlarmOptionsFragment extends DialogFragment {
    public static final String ID_KEY = "id_key";

    AlarmOptionsFragment.OnOptionSelectedListener mCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (AlarmOptionsFragment.OnOptionSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "must " +
                    "implement AlarmOptionsFragment.OnOptionSelectedListener!");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.alarm_options_title)
                .setItems(R.array.alarm_options_array, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        long id = getArguments().getLong(ID_KEY, 0);
                        switch (which) {
                            case 0:
                                mCallback.changeAlarm(id);
                                break;
                            case 1:
                                mCallback.deleteAlarm(id);
                                break;
                        }
                    }
                });

        return builder.create();
    }

    public interface OnOptionSelectedListener {
        void changeAlarm(long id);
        void deleteAlarm(long id);
    }
}
