package com.friendoye.alarmclock.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;

import java.util.Calendar;

// TODO: Specify Locale either ENGLISH or RUSSIAN
public class TimePickerFragment extends DialogFragment {
    public static final String TIME_KEY = "time_key";

    TimePickerDialog.OnTimeSetListener mCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (TimePickerDialog.OnTimeSetListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "must " +
                    "implement TimePickerDialog.OnTimeSetListener!");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(getArguments().getLong(TIME_KEY, 0));

        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);

        return new TimePickerDialog(getActivity(), mCallback,
                hours, minutes, true);
    }
}
