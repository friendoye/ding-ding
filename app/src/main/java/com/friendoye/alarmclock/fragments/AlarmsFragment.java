package com.friendoye.alarmclock.fragments;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.ListFragment;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CursorAdapter;

import com.friendoye.alarmclock.R;
import com.friendoye.alarmclock.database.DatabaseHelper;
import com.friendoye.alarmclock.holders.AlarmViewHolder;
import com.friendoye.alarmclock.utils.DifficultyType;
import com.friendoye.alarmclock.utils.DisableType;
import com.friendoye.alarmclock.utils.RussianDateFormat;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AlarmsFragment extends ListFragment {
    private OnAlarmActivityChanged mCallback;

    public AlarmsFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (OnAlarmActivityChanged) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "must " +
                    "implement OnAlarmActivityChanged!");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_alarms, container, false);
    }

    public void setCursor(Cursor cursor) {
        AlarmAdapter adapter = (AlarmAdapter) getListAdapter();
        if (getListAdapter() == null) {
            setListAdapter(new AlarmAdapter(getActivity(), cursor));
        } else {
            adapter.changeCursor(cursor);
        }
    }

    public interface OnAlarmActivityChanged {
        void onAlarmActivityChanged(int id, long time, boolean activated);
    }

    private class AlarmAdapter extends CursorAdapter {

        public AlarmAdapter(Context context, Cursor cursor) {
            super(context, cursor, false);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            final LayoutInflater inflater = LayoutInflater.from(context);
            View constructingView = inflater
                    .inflate(R.layout.view_alarm_info, parent, false);
            AlarmViewHolder holder = new AlarmViewHolder(constructingView);
            constructingView.setTag(holder);
            return constructingView;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            int id = cursor.getInt(cursor.getColumnIndex(
                    DatabaseHelper.ID));
            String description = cursor.getString(cursor.getColumnIndex(
                    DatabaseHelper.DESCRIPTION_KEY));
            String difficulty = cursor.getString(cursor.getColumnIndex(
                    DatabaseHelper.DIFFICULTY_KEY));
            String disable = cursor.getString(cursor.getColumnIndex(
                    DatabaseHelper.DISABLE_KEY));
            long time = cursor.getLong(cursor.getColumnIndex(
                    DatabaseHelper.TIME_KEY));
            boolean activated = cursor.getInt(cursor.getColumnIndex(
                    DatabaseHelper.ACTIVATED_KEY)) != 0;

            AlarmViewHolder holder = (AlarmViewHolder) view.getTag();

            holder.descriptionView.setText(description);

            int drawableId = DifficultyType.valueOf(difficulty).getDrawableId();
            holder.difficultyView.setImageDrawable(ContextCompat
                    .getDrawable(getActivity(), drawableId));

            drawableId = DisableType.valueOf(disable).getDrawableId();
            holder.disableView.setImageDrawable(ContextCompat
                    .getDrawable(getActivity(), drawableId));

            setTime(holder, time);

            holder.alarmSwitch.setTag(id);
            holder.alarmSwitch.setChecked(activated);
            holder.alarmSwitch.setOnCheckedChangeListener(
                    new SwitchListener(time));
            holder.alarmSwitch.setFocusable(false);

            view.setOnLongClickListener(new AlarmLongClickListener(id));
        }

        protected void setTime(AlarmViewHolder holder, long time) {
            Locale locale = Locale.getDefault();
            Date date = new Date(time);

            SimpleDateFormat formatter;
            if (locale.getLanguage().equals("ru")) {
                DateFormatSymbols dateFormat = new DateFormatSymbols();
                dateFormat.setShortMonths(RussianDateFormat.MONTH_NAMES);
                dateFormat.setShortWeekdays(RussianDateFormat.DAY_NAMES);
                formatter = new SimpleDateFormat("");
                formatter.setDateFormatSymbols(dateFormat);
            } else {
                formatter = new SimpleDateFormat("", Locale.ENGLISH);
            }

            final String[] formats = {
                    "HH:mm",
                    "d MMM yyyy",
                    "cccc"
            };

            formatter.applyPattern(formats[0]);
            holder.timeView.setText(formatter.format(date));

            formatter.applyPattern(formats[1]);
            holder.dateView.setText(formatter.format(date).toLowerCase());

            formatter.applyPattern(formats[2]);
            holder.dayView.setText(formatter.format(date));
        }
    }

    private class SwitchListener
            implements CompoundButton.OnCheckedChangeListener {
        private long mTime;

        public SwitchListener(long time) {
            mTime = time;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            mCallback.onAlarmActivityChanged(
                    (Integer) buttonView.getTag(),
                    mTime,
                    isChecked);
        }
    }

    private class AlarmLongClickListener
            implements View.OnLongClickListener {
        private long mId;

        public AlarmLongClickListener(long id) {
            this.mId = id;
        }

        @Override
        public boolean onLongClick(View v) {
            DialogFragment newFragment = new AlarmOptionsFragment();
            Bundle bundle = new Bundle();
            bundle.putLong(AlarmOptionsFragment.ID_KEY, mId);
            newFragment.setArguments(bundle);
            newFragment.show(getFragmentManager(), "alarmOptions");
            return false;
        }
    }
}
