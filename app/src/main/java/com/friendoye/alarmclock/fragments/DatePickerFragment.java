package com.friendoye.alarmclock.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Build;
import android.os.Bundle;

import java.util.Calendar;

// TODO: Specify Locale either ENGLISH or RUSSIAN
public class DatePickerFragment extends DialogFragment {
    public static final String DATE_KEY = "date_key";

    DatePickerDialog.OnDateSetListener mCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (DatePickerDialog.OnDateSetListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "must " +
                    "implement DatePickerDialog.OnDateSetListener!");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(getArguments().getLong(DATE_KEY, 0));

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return new DatePickerDialog(getActivity(),
                    android.R.style.Theme_Material_Dialog,
                    mCallback, year, month, dayOfMonth);
        } else {
            return new DatePickerDialog(getActivity(),
                    mCallback, year, month, dayOfMonth);
        }
    }
}
