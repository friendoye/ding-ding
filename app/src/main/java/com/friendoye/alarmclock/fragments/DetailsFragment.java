package com.friendoye.alarmclock.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.friendoye.alarmclock.R;
import com.friendoye.alarmclock.holders.DetailViewHolder;
import com.friendoye.alarmclock.utils.DifficultyType;
import com.friendoye.alarmclock.utils.DisableType;
import com.friendoye.alarmclock.utils.ResourcesContaining;

import java.util.LinkedList;

/**
 *
 */
public class DetailsFragment extends Fragment {
    public static final String DIFFICULTY_KEY = "dif_key";
    public static final String DISABLE_KEY = "dis_key";

    final DetailsAdapter adapter = new DetailsAdapter();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences prefs =
                PreferenceManager.getDefaultSharedPreferences(getActivity());
        int position = 0;

        adapter.addCategory(getString(R.string.difficulty_text),
                DifficultyType.values(),
                DIFFICULTY_KEY);
        String difficulty = prefs.getString(DIFFICULTY_KEY, null);
        if (difficulty != null) {
            position = DifficultyType.valueOf(difficulty).ordinal();
        }
        adapter.setSelected(DIFFICULTY_KEY, position);

        adapter.addCategory(getString(R.string.disable_text),
                DisableType.values(),
                DISABLE_KEY);
        String disable = prefs.getString(DISABLE_KEY, null);
        if (disable != null) {
            position = DisableType.valueOf(disable).ordinal();
        } else {
            position = 0;
        }
        adapter.setSelected(DISABLE_KEY, position);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Activity ownerActivity = getActivity();
        ListView list = (ListView)
                ownerActivity.findViewById(R.id.detailsListView);

        adapter.setContext(ownerActivity);

        list.setDivider(null);
        list.setAdapter(adapter);
    }

    @Override
    public void onStop() {
        super.onStop();

        SharedPreferences prefs =
                PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = prefs.edit();

        DifficultyType difficultyType = (DifficultyType)
                adapter.getSelected(DIFFICULTY_KEY);
        editor.putString(DIFFICULTY_KEY, difficultyType.toString());

        DisableType disableType = (DisableType)
                adapter.getSelected(DISABLE_KEY);
        editor.putString(DISABLE_KEY, disableType.toString());

        editor.apply();
    }

    private class DetailsAdapter extends BaseAdapter
            implements CompoundButton.OnClickListener {
        private LinkedList<Category> categoryList;
        private Context mContext;

        public DetailsAdapter() {
            categoryList = new LinkedList<>();
        }

        public void addCategory(String headerTitle,
                                ResourcesContaining[] items,
                                String categoryKey) {
            Category cat = new Category();
            cat.headerTitle = headerTitle;
            cat.items = items;
            cat.key = categoryKey;
            cat.chosenPosition = getCount() + 1;
            categoryList.add(cat);
        }

        public void setContext(Context context) {
            mContext = context;
        }

        public ResourcesContaining getSelected(String categoryKey) {
            int offset = 0;
            for (Category cat : categoryList) {
                if (cat.key.equals(categoryKey)) {
                    return cat.items[cat.chosenPosition - offset - 1];
                } else {
                    offset += cat.items.length + 1;
                }
            }
            return null;
        }

        public void setSelected(String categoryKey, int index) {
            int offset = 0;
            for (Category cat : categoryList) {
                if (cat.key.equals(categoryKey)) {
                    cat.chosenPosition = offset + 1 + index;
                    break;
                } else {
                    offset += cat.items.length + 1;
                }
            }
        }

        @Override
        public int getCount() {
            if (categoryList == null || categoryList.size() == 0) {
                return 0;
            }
            int amount = 0;
            for (Category cat : categoryList) {
                amount += 1 + cat.items.length;
            }
            return amount;
        }

        @Override
        public Object getItem(int position) {
            int offset = position;
            for (Category cat : categoryList) {
                if (offset == 0) {
                    // Header
                    return cat.headerTitle;
                }
                offset--;
                if (offset < cat.items.length) {
                    // Item
                    return cat.items[offset];
                }
                offset -= cat.items.length;
            }
            return null;
        }

        private Category getCategory(int position) {
            int offset = position;
            for (Category cat : categoryList) {
                if (offset < cat.items.length + 1) {
                    return cat;
                }
                offset -= cat.items.length + 1;
            }
            throw new RuntimeException("Cannot find Category for pos = "
                    + position);
        }

        @Override
        public long getItemId(int position) {
            return position * 47 + 11;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            return getItem(position) instanceof String ? 0 : 1;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return !(getItem(position) instanceof String);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (getItemViewType(position) == 0) {
                return getHeaderView(position, convertView, parent);
            }

            DetailViewHolder holder;
            if (convertView == null) {
                convertView = getActivity().getLayoutInflater()
                        .inflate(R.layout.view_details_item, parent, false);
                holder = new DetailViewHolder(convertView);
                convertView.setTag(holder);
                convertView.setOnClickListener(this);
            } else {
                holder = (DetailViewHolder) convertView.getTag();
            }

            ResourcesContaining item = (ResourcesContaining) getItem(position);
            Category cat = getCategory(position);

            Drawable drawableIcon = ContextCompat.getDrawable(mContext,
                    item.getDrawableId());
            holder.imageView.setImageDrawable(drawableIcon);

            String text = getString(item.getStringId());
            holder.textView.setText(text);

            holder.radioButton.setTag(position);
            holder.radioButton.setChecked(position == cat.chosenPosition);
            holder.radioButton.setOnClickListener(this);

            return convertView;
        }

        public View getHeaderView(int position, View convertView,
                                  ViewGroup parent) {
            if (convertView == null) {
                convertView = getActivity().getLayoutInflater()
                        .inflate(R.layout.view_details_header, parent, false);
            }

            TextView title = (TextView) convertView.findViewById(
                    R.id.headerTextView);
            title.setText((String) getItem(position));

            return convertView;
        }

        @Override
        public void onClick(View view) {
            Object tag = view.getTag();
            int position;
            if (tag instanceof DetailViewHolder) {
                DetailViewHolder holder = (DetailViewHolder) tag;
                position = (Integer) holder.radioButton.getTag();
            } else {
                position = (Integer) tag;
            }
            Category cat = getCategory(position);
            cat.chosenPosition = position;
            notifyDataSetChanged();
        }

        private class Category {
            String headerTitle;
            ResourcesContaining[] items;
            String key;
            int chosenPosition = 0;
        }
    }
}
