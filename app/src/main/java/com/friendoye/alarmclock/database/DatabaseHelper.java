package com.friendoye.alarmclock.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.friendoye.alarmclock.utils.Alarm;
import com.friendoye.alarmclock.utils.DifficultyType;
import com.friendoye.alarmclock.utils.DisableType;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String ID = "_id";
    public static final String DESCRIPTION_KEY = "description";
    public static final String DIFFICULTY_KEY = "difficulty";
    public static final String DISABLE_KEY = "disable";
    public static final String TIME_KEY = "time";
    public static final String REPEAT_KEY = "repeat";
    public static final String MELODY_KEY = "melody";
    public static final String ACTIVATED_KEY = "activated";

    private static final String DATABASE_NAME = "alarms.db";
    private static final String ALARMS_TABLE_NAME = "ALARMS";
    private static final String SQL_CREATE_TABLE_QUERY =
            "CREATE TABLE if not exists ALARMS (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "description TEXT, difficulty TEXT, disable TEXT, time LONG, " +
                    "repeat BYTE, melody TEXT, activated BYTE);";

    private static final int SCHEMA = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, SCHEMA);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        throw new UnsupportedOperationException("Schema version is 1. " +
                "Why is it called?");
    }

    public synchronized long insertAlarm(Alarm alarm) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues entry = createEntryFromAlarm(alarm);
        return database.insert(ALARMS_TABLE_NAME, null, entry);
    }

    public synchronized void updateAlarm(long id, Alarm alarm) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues entry = createEntryFromAlarm(alarm);
        database.update(ALARMS_TABLE_NAME, entry,
                ID + "=" + id, null);
        database.close();
    }

    protected ContentValues createEntryFromAlarm(Alarm alarm) {
        ContentValues entry = new ContentValues();

        entry.put(DESCRIPTION_KEY, alarm.description);
        entry.put(DIFFICULTY_KEY, alarm.difficulty.toString());
        entry.put(DISABLE_KEY, alarm.disable.toString());
        entry.put(TIME_KEY, alarm.time);
        entry.put(REPEAT_KEY, alarm.repeat ? 1 : 0);
        entry.put(MELODY_KEY, alarm.melody);
        entry.put(ACTIVATED_KEY, alarm.activated ? 1 : 0);

        return entry;
    }

    public synchronized boolean hasAlarms() {
        final String countQuery = "SELECT count(*) FROM " + ALARMS_TABLE_NAME;
        Cursor cursor = getReadableDatabase().rawQuery(countQuery, null);
        cursor.moveToFirst();
        return cursor.getInt(0) > 0;
    }

    public synchronized Cursor getAlarms() {
        final String[] columns = new String[]{
                ID,
                DESCRIPTION_KEY,
                DIFFICULTY_KEY,
                DISABLE_KEY,
                TIME_KEY,
                MELODY_KEY,
                REPEAT_KEY,
                ACTIVATED_KEY};
        return getReadableDatabase().query(ALARMS_TABLE_NAME,
                columns, null, null, null, null, null, null);
    }

    public synchronized Alarm getAlarm(long id) {
        final String[] columns = new String[]{
                ID,
                DESCRIPTION_KEY,
                DIFFICULTY_KEY,
                DISABLE_KEY,
                TIME_KEY,
                MELODY_KEY,
                REPEAT_KEY,
                ACTIVATED_KEY};
        Cursor cursor = getReadableDatabase().query(ALARMS_TABLE_NAME,
                columns, ID + "=?", new String[]{id + ""}, null, null, null);

        Alarm alarm = new Alarm();
        cursor.moveToFirst();
        alarm.description = cursor.getString(cursor.getColumnIndex(
                DatabaseHelper.DESCRIPTION_KEY));
        alarm.difficulty = DifficultyType.valueOf(cursor.getString(
                cursor.getColumnIndex(DatabaseHelper.DIFFICULTY_KEY)));
        alarm.disable = DisableType.valueOf(cursor.getString(
                cursor.getColumnIndex(DatabaseHelper.DISABLE_KEY)));
        alarm.time = cursor.getLong(cursor.getColumnIndex(
                DatabaseHelper.TIME_KEY));
        alarm.activated = cursor.getInt(cursor.getColumnIndex(
                DatabaseHelper.ACTIVATED_KEY)) != 0;
        alarm.repeat = cursor.getInt(cursor.getColumnIndex(
                DatabaseHelper.REPEAT_KEY)) != 0;
        alarm.melody = cursor.getString(cursor.getColumnIndex(
                DatabaseHelper.MELODY_KEY));

        cursor.close();

        return alarm;
    }

    public synchronized void updateAlarmActivity(long id, boolean activated) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues entry = new ContentValues();

        entry.put(ACTIVATED_KEY, activated);
        database.update(ALARMS_TABLE_NAME, entry,
                ID + "=" + id, null);

        database.close();
    }

    public synchronized void deleteAlarm(long id) {
        SQLiteDatabase database = getWritableDatabase();
        database.delete(ALARMS_TABLE_NAME, ID + "=" + id, null);
        database.close();
    }
}
