package com.friendoye.alarmclock.holders;

import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.friendoye.alarmclock.R;

public class AlarmViewHolder {
    public ImageView disableView;
    public ImageView difficultyView;
    public TextView timeView;
    public TextView dateView;
    public TextView descriptionView;
    public TextView dayView;
    public SwitchCompat alarmSwitch;

    public AlarmViewHolder(View parentView) {
        disableView = (ImageView) parentView.findViewById(R.id.disableView);
        difficultyView = (ImageView) parentView.findViewById(R.id.difficultyView);
        timeView = (TextView) parentView.findViewById(R.id.timeView);
        dateView = (TextView) parentView.findViewById(R.id.dateView);
        descriptionView = (TextView) parentView.findViewById(R.id.descriptionView);
        dayView = (TextView) parentView.findViewById(R.id.dayView);
        alarmSwitch = (SwitchCompat) parentView.findViewById(R.id.alarmSwitch);
    }
}
