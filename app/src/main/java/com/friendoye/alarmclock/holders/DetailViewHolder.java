package com.friendoye.alarmclock.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.friendoye.alarmclock.R;

public class DetailViewHolder {
    public ImageView imageView;
    public TextView textView;
    public RadioButton radioButton;

    public DetailViewHolder(View parentView) {
        imageView = (ImageView) parentView.findViewById(R.id.imageView);
        textView = (TextView) parentView.findViewById(R.id.textView);
        radioButton = (RadioButton) parentView.findViewById(R.id.radioButton);
    }
}
