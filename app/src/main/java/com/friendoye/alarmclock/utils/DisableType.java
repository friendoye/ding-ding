package com.friendoye.alarmclock.utils;

import com.friendoye.alarmclock.R;

public enum DisableType implements ResourcesContaining {
    MATHEMATICAL,
    LINKING,
    RANDOMLY;

    public static DisableType getDefault() {
        return MATHEMATICAL;
    }

    public int getStringId() {
        switch (this) {
            case MATHEMATICAL:
                return R.string.mathematical_text;
            case LINKING:
                return R.string.linking_text;
            case RANDOMLY:
                return R.string.randomly_text;
        }
        throw new UnsupportedOperationException("Unexpected DisableType!");
    }

    public int getDrawableId() {
        switch (this) {
            case MATHEMATICAL:
                return R.drawable.mathematical;
            case LINKING:
                return R.drawable.linking;
            case RANDOMLY:
                return R.drawable.randomly;
        }
        throw new UnsupportedOperationException("Unexpected DisableType!");
    }
}
