package com.friendoye.alarmclock.utils;

import com.friendoye.alarmclock.R;

public enum DifficultyType implements ResourcesContaining {
    EASY,
    MEDIUM,
    HARD;

    public static DifficultyType getDefault() {
        return EASY;
    }

    public int getStringId() {
        switch (this) {
            case EASY:
                return R.string.easy_text;
            case MEDIUM:
                return R.string.medium_text;
            case HARD:
                return R.string.hard_text;
        }
        throw new UnsupportedOperationException("Unexpected DifficultyType!");
    }

    public int getDrawableId() {
        switch (this) {
            case EASY:
                return R.drawable.easy;
            case MEDIUM:
                return R.drawable.medium;
            case HARD:
                return R.drawable.hard;
        }
        throw new UnsupportedOperationException("Unexpected DifficultyType!");
    }
}
