package com.friendoye.alarmclock.utils;

public interface ResourcesContaining {
    int getStringId();

    int getDrawableId();
}
