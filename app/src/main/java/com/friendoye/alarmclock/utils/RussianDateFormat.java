package com.friendoye.alarmclock.utils;

public class RussianDateFormat {
    public static final String[] MONTH_NAMES = new String[]{
            "Янв",
            "Фев",
            "Мар",
            "Апр",
            "Май",
            "Июн",
            "Июл",
            "Авг",
            "Сен",
            "Окт",
            "Ноя",
            "Дек"
    };

    public static final String[] DAY_NAMES = new String[]{
            "", "Вс",
            "Пн", "Вт",
            "Ср", "Чт",
            "Пт", "Сб"
    };
}
