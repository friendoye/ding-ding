package com.friendoye.alarmclock.utils;

public class Alarm {
    public String description;
    public String melody;
    public DifficultyType difficulty;
    public DisableType disable;
    public boolean repeat;
    public boolean activated;
    public long time;
    public int id;
}
